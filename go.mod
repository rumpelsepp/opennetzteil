module codeberg.org/rumpelsepp/opennetzteil

go 1.17

require (
	codeberg.org/rumpelsepp/helpers v0.0.0-20211020091314-b9b064cf8c8a
	git.sr.ht/~sircmpwn/getopt v1.0.0
	github.com/Fraunhofer-AISEC/penlogger v0.0.0-20210914113712-8a2b1758b080
	github.com/felixge/httpsnoop v1.0.2 // indirect
	github.com/gizak/termui/v3 v3.1.0
	github.com/google/uuid v1.3.0 // indirect
	github.com/gorilla/handlers v1.5.1
	github.com/gorilla/mux v1.8.0
	github.com/gorilla/websocket v1.5.0
	github.com/jacobsa/go-serial v0.0.0-20180131005756-15cf729a72d4
	github.com/pelletier/go-toml v1.9.4
	github.com/spf13/pflag v1.0.5
	golang.org/x/crypto v0.0.0-20220331220935-ae2d96664a29 // indirect
	golang.org/x/sys v0.0.0-20220406163625-3f8b81556e12 // indirect
)

require (
	github.com/coreos/go-systemd v0.0.0-20191104093116-d3cd4ed1dbcf // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/mattn/go-runewidth v0.0.2 // indirect
	github.com/mitchellh/go-wordwrap v0.0.0-20150314170334-ad45545899c7 // indirect
	github.com/nsf/termbox-go v0.0.0-20190121233118-02980233997d // indirect
)
